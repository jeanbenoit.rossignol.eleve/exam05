#pragma once 

#include <iostream>
class ATarget;

class ASpell
{
protected:
    std::string _name;
    std::string _effects;
private:
    ASpell(const ASpell &);
    ASpell& operator=(const ASpell &);

public:
    ASpell(const std::string &, const std::string &);
    virtual ~ASpell();

    const std::string & getName() const;
    const std::string & getEffects() const;
    void launch(const ATarget &) const;

    virtual ASpell * clone() const = 0;
};

#include "ATarget.hpp"