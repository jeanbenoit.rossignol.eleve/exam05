#pragma once

#include <iostream>
class ASpell;

class ATarget
{
protected:
    std::string _type;
private:
    ATarget(const ATarget &);
    ATarget & operator=(const ATarget &);

public:
    ATarget(const std::string &);
    virtual ~ATarget();

    const std::string & getType() const;
    void getHitBySpell(const ASpell &) const;

    virtual ATarget * clone() const = 0;
};

#include "ASpell.hpp"