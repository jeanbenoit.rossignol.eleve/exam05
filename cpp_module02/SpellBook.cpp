#include "SpellBook.hpp"

SpellBook::SpellBook() {}

SpellBook::~SpellBook() {}


void SpellBook::learnSpell(ASpell * spell) {
    _spellbook.insert(std::make_pair(spell->getName(), spell->clone()));
}

void SpellBook::forgetSpell(const std::string & spell) {
    _spellbook.erase(spell);
}

ASpell * SpellBook::createSpell(const std::string & spell) {
    std::map<std::string, ASpell *>::iterator it = _spellbook.find(spell);
    if (it == _spellbook.end())
        return NULL;
    return it->second->clone();
}