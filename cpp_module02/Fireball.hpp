#pragma once

#include "ASpell.hpp"

class Fireball: public ASpell
{
private:
public:
    Fireball();
    ~Fireball();
    Fireball * clone() const;
};