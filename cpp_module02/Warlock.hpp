#pragma once

#include <iostream>
#include <map>
#include "ASpell.hpp"
#include "SpellBook.hpp"

class Warlock {
    public:
        Warlock(const std::string &, const std::string &);
        ~Warlock();
        const std::string & getName() const;
        const std::string & getTitle() const;
        void setTitle(const std::string &);
        void introduce() const;
        void learnSpell(ASpell *);
        void forgetSpell(std::string);
        void launchSpell(std::string, const ATarget &);
    private:
        std::string _name;
        std::string _title;
        SpellBook * _spellbook;
        Warlock(const Warlock &);
        Warlock & operator=(const Warlock &);
};