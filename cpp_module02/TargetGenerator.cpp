#include "TargetGenerator.hpp"

TargetGenerator::TargetGenerator() {}

TargetGenerator::~TargetGenerator() {}

void TargetGenerator::learnTargetType(ATarget * target) {
    _targetbook.insert(std::make_pair(target->getType(), target));
}

void TargetGenerator::forgetTargetType(const std::string & target) {
    _targetbook.erase(target);
}

ATarget * TargetGenerator::createTarget(const std::string & target) {
    std::map<std::string, ATarget *>::iterator it = _targetbook.find(target);
    if (it == _targetbook.end())
    {
        return NULL;
    }
    return it->second->clone();
}