#pragma once

#include "ATarget.hpp"

class BrickWall: public ATarget
{
private:
public:
    BrickWall();
    ~BrickWall();
    BrickWall * clone() const;
};