#pragma once

#include <iostream>
#include <map>
#include "ATarget.hpp"

class TargetGenerator
{
private:
    TargetGenerator(const TargetGenerator &);
    TargetGenerator & operator=(const TargetGenerator &);

    std::map<std::string, ATarget *> _targetbook;
public:
    TargetGenerator();
    ~TargetGenerator();
    void learnTargetType(ATarget *);
    void forgetTargetType(const std::string &);
    ATarget * createTarget(const std::string &);
};