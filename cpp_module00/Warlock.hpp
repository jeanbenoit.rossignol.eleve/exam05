#pragma once

#include <iostream>

class Warlock {
    public:
        Warlock(const std::string &, const std::string &);
        ~Warlock();
        const std::string & getName() const;
        const std::string & getTitle() const;
        void setTitle(const std::string &);
        void introduce() const;
    private:
        std::string _name;
        std::string _title;
        Warlock(const Warlock &);
        Warlock & operator=(const Warlock &);
};